# long-ex-exo-monoz

Code is in the following link: [CMSDAS-MonoZ-Tutorial-2024](https://github.com/yhaddad/CMSDAS-MonoZ-Tutorial-2024/tree/main)
The documentation can be found here: [MonoZ Analysis documentation](https://submit.mit.edu/~freerc/MonoZ_CMSDAS/index.html)
